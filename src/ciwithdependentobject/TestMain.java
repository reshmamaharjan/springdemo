package ciwithdependentobject;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

public class TestMain {
    public static void main(String[] args) {
        BeanFactory b=new XmlBeanFactory(new ClassPathResource("ciwithdependentobject/applicationContext.xml"));
        Question q=(Question)b.getBean("question");
        q.display();

    }
}
