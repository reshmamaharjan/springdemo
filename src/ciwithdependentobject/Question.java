package ciwithdependentobject;

import java.util.Iterator;
import java.util.List;

public class Question {
    private int id;
    private String name;
    private List<Answer> answers;
    Question(int id,String name,List<Answer> answers){
        this.id=id;
        this.name=name;
        this.answers=answers;
    }
    public void display(){
        System.out.println("ID:"+id+" Name:"+name);
        System.out.println("Answers are:");
        Iterator<Answer> it=answers.iterator();
        while(it.hasNext()){
            //Answer a=it.next();
            System.out.println("\n"+it.next());
        }
    }
}
