package constructioncollection;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class TestMain {
    public static void main(String[] args) {
        Resource r=new ClassPathResource("constructioncollection/applicationContext.xml");
        BeanFactory b=new XmlBeanFactory(r);
        //BeanFactory b=new XmlBeanFactory(new ClassPathResource("constructorcollection/applicationContext.xml"));
        Question q=(Question)b.getBean("bid");
        q.display();
    }
}
