package constructioncollection;
import java.util.*;

public class Question {
    private int id;
    private String name;
    List<String> answers;

    public Question(int id,String name,List<String> answers){
        this.id=id;
        this.name=name;
        this.answers=answers;
    }
    public void display(){
        System.out.println("Id:"+id+"Name:"+name);
        System.out.println("Answers are:");
        Iterator<String> it=answers.iterator();
        while(it.hasNext()){
            System.out.println("\n"+it.next());
        }

    }


}
