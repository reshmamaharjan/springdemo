import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;

import java.sql.SQLOutput;

public class StudentInfo {
    public static void main(String[] args) {
        BeanFactory obj=new XmlBeanFactory(new FileSystemResource("std.xml"));
        Student s=(Student) obj.getBean("sid");
        System.out.println(s.getId()+"  "+s.getName());
    }
}
