package dibyconstructor;

public class Employee {
    private int id;
    private String name;

    public Employee(){
        System.out.println("Default Constructor");
    }
    public Employee(int id){
        this.id=id;
    }
    public Employee(int id,String name){
        this.id=id;
        this.name=name;
    }
    void display(){
        System.out.println("Employee details Id is:"+id+"\t"+"Name is:"+name);
    }

}
