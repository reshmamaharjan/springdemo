package dibyconstructor;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.*;

public class Test {
    public static void main(String[] args) {
        //BeanFactory bf=new XmlBeanFactory(new ClassPathResource(applicationContext.xml));
        Resource r=new ClassPathResource("dibyconstructor/applicationContext.xml");
        BeanFactory b=new XmlBeanFactory(r);
        Employee s=(Employee)b.getBean("e");
        s.display();
    }
}
